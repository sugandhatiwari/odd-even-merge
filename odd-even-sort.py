f = open("sort_odd_even_merge.bsv", "w")

def compare_swap(a,b):
   f.write("\tif(number[" + str(a) +"]>number["+ str(b)+"]) begin\n")
   f.write("\tlet temp = number["+str(a)+"];\n")
   f.write("\tnumber["+str(a)+"]= number["+str(b)+"];\n")
   f.write("\tnumber["+str(b)+"] = temp;\n")
   f.write("\tend\n")
   return;

def odd_even_swap(n,a,b):
   f.write("\tnumber = odd_even_swap(number,"+str(a)+","+str(b)+");\n")
   return;

def odd_even_merge(n,a,b):
   f.write("\tnumber = odd_even_merge(number,"+str(a)+","+str(b)+");\n")
   return;

# Recusive implementation of Merging
def merge(n,a,b):
   if n==2:
     compare_swap(a,b)
     return
   odd_even_swap(n,a,b)
   merge(n/2,a,(a+b)/2)
   merge(n/2,(a+b)/2+1,b)
   odd_even_merge(n,a,b)
   return;

# Recusive implementation of Sorting
def sort_num(n,a,b):
   if n==2:
	compare_swap(a,b)
	return
   sort_num(n/2,a,(a+b)/2)
   sort_num(n/2,(a+b)/2+1,b)
   merge(n,a,b)
   return;

string1  = """ 
package sort_odd_even_merge;
import Vector::*;
"""
f.write(string1+"\n")

print("Enter the number of elements (power of 2)")
num = input()

f.write("`define num " + str(num))

string2  = """ 
interface Ifc_sort_odd_even_merge;
   method Vector#(`num,int) odd_even_sort(Vector#(`num,int) numbers);
endinterface
	
module mkodd_even_merge(Ifc_sort_odd_even_merge);
	// Function for Odd-Even Swap (Used In Recursive Merge)
	function Vector#(`num,int) odd_even_swap(Vector#(`num,int) number,int start_in,int end_in);
		Vector#(`num,int) a;
		a[end_in] = number[end_in];
		a[start_in] = number[start_in];
		int j=start_in+1;
		for(int i=start_in+2;i<end_in;i=i+2) begin
			a[j] = number[i];
			j=j+1;
		end
		for(int i = start_in+1;i<end_in;i=i+2) begin
			a[j] = number[i];
			j=j+1;
		end
	        for(int i = start_in; i < end_in; i =i +1) begin
			number[i] = a[i];
		end
		return number;
	endfunction

	// Function for Odd-Even Merge (Used In Recursive Merge)
	function Vector#(`num,int) odd_even_merge(Vector#(`num,int) number,int start_in,int end_in);
		Vector#(`num,int) a;
		a[end_in] = number[end_in];
		a[start_in] = number[start_in];
		int index = ((start_in+end_in)/2)+1;
		int j = start_in;
		for(int i=start_in+1; index<end_in; index=index+1,i=i+1) begin
			if(number[i]>number[index]) begin
			    a[j+1] = number[index];
			    a[j+2] = number[i];
			end
			else begin
			    a[j+1] = number[i];
			    a[j+2] = number[index];
			end
			j = j + 2;
		end
		for(int i = start_in; i < end_in; i =i+1) begin
			number[i] = a[i];
		end
		return number;
	endfunction

	function Vector#(`num,int) sort_num(Vector#(`num,int) number,int start_in,int end_in,int n);
"""
f.write(string2+"\n")

# Recursive Function call
sort_num(num,0,num-1)

	
string3  = """ 
	return number;
	endfunction

	method Vector#(`num,int) odd_even_sort(Vector#(`num,int) numbers);
	  Vector#(`num,int)  x;
	  for(int i=0;i<`num;i=i+1) begin
	    x[i] = numbers[i];
	  end

          Vector#(`num,int) a = sort_num(x,0,`num-1,`num);
	 return a;
        endmethod

endmodule
	
endpackage
"""
f.write(string3+"\n")
