f = open("sortTestbench.bsv", "w")

print("Enter the number of elements (power of 2)")
num = input()

string1  = """ 
package sortTestbench;
import Vector::*;
import sort_odd_even_merge::*;
"""
f.write(string1+"\n")

f.write("`define num " + str(num))

string2  = """ 
module mkTestbench();
	Reg#(int) count <- mkReg(0);
	Reg#(int) state <-mkReg(0);
	
	Ifc_sort_odd_even_merge m <- mkodd_even_merge;

	rule go(state==0);
	  Vector#(`num,int) x,outputs;
"""
f.write(string2+"\n")

for i in range(num):
  k = input("Value at index"+str(i)+" : ")
  f.write("\tx["+ str(i) + "] = " + str(k) + ";\n")

string3  = """ 	
	  $display("%0d : Array Elements Before Sorting...........",$time);
	  for(int i=0;i<`num;i=i+1) begin
		$display("%d",x[i]);
	  end

	  $display("%0d : Perform Odd-Even Sorting...........",$time);
	  outputs = m.odd_even_sort(x);
	
	  $display("%0d :Array Elements After Sorting...........",$time);
	  for(int i=0;i<`num;i=i+1) begin
		$display("%d",outputs[i]);
	  end
	  state <= 1;
	endrule

	rule fin(state==1);
		$display("..........Completed Successfully.........");
		$finish;
	endrule

endmodule
endpackage
"""
f.write(string3+"\n")
f.close();
